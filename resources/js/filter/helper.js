export const checkFilter = () => {
    const  filter = $('#pe_config_container');

    return !!filter.is(':visible');
}

export const getFilterData = () => {
    const  filter = $('#pe_config_container');

    return filter.find('select, input').serialize();
}


export const getValuesFor = () => {
    let value = $('#search_filter').val();
    let data =  {
        term: value
    }
    if(checkFilter())
        data.filter  =  getFilterData();

    return data
}