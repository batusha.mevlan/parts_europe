import {
    autoComplete,
    checkSearchInput,
    clearSearchInput,
    loadMoreData,
    searchPartVehicle,
    showFilter} from "./action";

$(document).ready(function () {

    const scope         = $(document);
    const input_search  = $("#search_filter");


    scope.on('submit', '#pe_search_part_vehicle', searchPartVehicle)

    scope.on('click', '#pe_load_more_js', loadMoreData)

    scope.on('click', '#pe_icon_filter_js', showFilter)

    scope.on('click', '#pe_icon_filter_times_js', clearSearchInput)


    input_search.hover(checkSearchInput);
    input_search.autocomplete({
        source: function( request, response ) {
            $.ajax( {
                type: 'get',
                url: "/search_autocomplete",
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function( data ) {
                    response( data );
                }
            } );
        },
        minLength: 2,
        select: function( event, ui ) {
            autoComplete(ui.item.value);
        }
    });

});