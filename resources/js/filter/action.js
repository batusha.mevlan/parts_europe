import {checkFilter, getValuesFor} from "./helper";


export function autoComplete(item) {
    const containerLoad = $('.pe_container_js');
    let data = getValuesFor();
    data.term = item;


    $.ajax({
        type: 'get',
        url: '/search_parts',
        dataType:'json',
        data: data,
        headers: {'X-Requested-With': 'XMLHttpRequest'},
        success: function (data) {
            containerLoad.empty().append(data.view);
        }
    })
}

export function searchPartVehicle(e) {
    e.preventDefault();
    const containerLoad = $('.pe_container_js');

    $.ajax({
        type: 'get',
        url: '/search_parts',
        dataType:'json',
        data: getValuesFor(),
        headers: {'X-Requested-With': 'XMLHttpRequest'},
        success: function (data) {
            containerLoad.empty().append(data.view);
        }
    })
}

export function loadMoreData(e) {
    e.preventDefault();

    let self = $(this);
    const containerLoad = $('.pe_container_js');
    let data = getValuesFor()
    data.page = self.data('loader');

    $.ajax({
        type: 'get',
        url: '/load_more',
        data: data,
        headers: {'X-Requested-With': 'XMLHttpRequest'},
        success: function (data) {
            containerLoad.append(data);
        },
        complete: function () {
            self.data('loader', parseInt(data.page) + 1);
        }
    })
}


export function clearSearchInput(e) {
    let self = $(this);
    e.preventDefault();
    let parent = self.closest('#pe_search_part_vehicle');

    parent.find('#search_filter').val('');
    parent.find('.pe_search_part_vehicle').trigger('click');
}


export function checkSearchInput() {
    let self    = $(this);
    let parent  = self.closest('#pe_search_part_vehicle');

    if (self.val() !== '') {
        parent.find('#pe_icon_filter_times_js').show();
    }
    else
        parent.find('#pe_icon_filter_times_js').hide();
}

export function showFilter() {
   const filter = $('#pe_config_container');

   if (checkFilter())
       filter.hide()
   else
        filter.show()
}

