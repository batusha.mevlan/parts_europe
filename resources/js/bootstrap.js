window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.$ = window.jQuery = require( "jquery" );

require("jquery-ui/dist/jquery-ui.min");

require('bootstrap')