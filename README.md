# parts_europe

## Getting started
It is done on Codeiginter 4 

Mix compiler forphp ECMA 5 and SCSS

Bootstrap 4

jQuery and  UI

## Installation

- [ ] First we have to create database "parts_europe" and Exacute SQL file (parts_europe.sql) to database, or we can update .env file.
- [ ] After its composer install (we install libraries for CI)
- [ ] npm install (javascript libraries)
- [ ] npm run dev or watch (we compile the code)
- [ ] php spark serve (we run server)
- [ ] localhost:8080 -> and we are good to go.
