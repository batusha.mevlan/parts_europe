<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Models\Vehicle;
use App\Models\Parts;

use Opt;
use Filter;

class Search extends Model
{
    protected $table   = 'vehicle';

    public function partsVehicle($search, $filter): array
    {
        $vehicleModel  = new Vehicle();
        $vehicle_parts = $vehicleModel->checkVehicle($search, $filter);

        $result = [];
        if (count($vehicle_parts) > 0) {
            $parts = new Parts();

            $isActive = 1;
            if (isset($filter[Filter::NotActive]))
                $isActive = $filter[Filter::NotActive] == 'on'? 0 : 1;

            $result = $parts->searchPartsFromVehicle($vehicle_parts, $isActive);
        }

        return $result;
    }

    public function more($search, $filter, $page): array
    {
        $vehicleModel  = new Vehicle();
        $vehicle_parts = $vehicleModel->checkVehicle($search, $filter);
        $result = [];

        if (count($vehicle_parts) > 0) {
            $parts  = new Parts();
            $offset = $page * Opt::PerPage;

            $isActive = 1;
            if (isset($filter[Filter::NotActive]))
                $isActive = $filter[Filter::NotActive] == 'on'? 0 : 1;

            $result = $parts->searchPartsFromVehicle($vehicle_parts, $isActive, $offset);
        }

        return $result;
    }
}
