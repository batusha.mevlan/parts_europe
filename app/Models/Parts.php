<?php

namespace App\Models;

use CodeIgniter\Model;
use Opt;
use Filter;

class Parts extends Model
{
    protected $table = 'parts';

    public function getData($offset_page = 0)
    {
        $db = db_connect();
        $query = $db->table($this->table);
        $res = $query
            ->select('parts_id AS id, parts_name AS name, vehicle_model AS model')
            ->select('vehicle_year AS year, vehicle_category_id AS category')
            ->join('vehicle', 'parts.parts_vehicle_ids = vehicle.vehicle_unique')
            ->where('parts_active', 1)
//            ->orderBy('RAND()')
            ->limit(Opt::PerPage, $offset_page)
            ->get();

        $result = $res->getResult();

        if (count($result))
            return $result;
        else
            return false;
    }

    public function searchPartsFromVehicle($vehicle_parts, $isActive, $offset_page = 0): array
    {

        $db = db_connect();
        $query = $db->table($this->table);
         $query
            ->select('parts_id AS id, parts_name AS name, vehicle_model AS model')
            ->select('vehicle_year AS year, vehicle_category_id AS category')
            ->join('vehicle', 'parts.parts_vehicle_ids = vehicle.vehicle_unique')
            ->where('parts_active', $isActive);

            if(!empty($vehicle_parts))
                $query->whereIn('parts.parts_vehicle_ids', array_values(array_column($vehicle_parts, 'id')));

        $res = $query->limit(Opt::PerPage, $offset_page)
            ->get();

        return $res->getResult();
    }
}
