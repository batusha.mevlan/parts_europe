<?php

namespace App\Models;

use CodeIgniter\Model;
use Filter;

class Vehicle extends Model
{
    protected $table = 'vehicle';

    public function checkVehicle($term, $filter = null): array
    {
        $db = db_connect();
        $query = $db->table($this->table);

        if (!empty($term)) {
            $query->select('vehicle_unique AS id')
                ->where("CONCAT(vehicle_producer, ', ', vehicle_model, ', ', vehicle_size, ', ', vehicle_year)", $term);
            $res = $this->getRes($filter, $query);

            if (empty($res)) {
                $query->select('vehicle_unique AS id')
                    ->like('vehicle_producer', $this->checkTerm($term));
                $res = $this->getRes($filter, $query);
            }
        }
        else {
            $query->select('vehicle_unique AS id');
            $res = $this->getRes($filter, $query);
        }

        return  $res;
    }

    public function searchVehicle($term): array
    {
        $db = db_connect();
        $query = $db->table($this->table);
        $res = $query
            ->select("vehicle_unique AS id")
            ->select("CONCAT(vehicle_producer, ', ', vehicle_model, ', ', vehicle_size, ', ', vehicle_year) AS value")
            ->select("CONCAT(vehicle_producer, ', ', vehicle_model, ', ', vehicle_size, ', ', vehicle_year) AS label")
            ->like("vehicle_producer", $term)
            ->limit(20)
            ->get();

        return $res->getResult();
    }



    #region Filter Data
    public function getFilterData(): array
    {
        $filter['categories']       = $this->getCategories();
        $filter['subcategories']    = $this->getSubcategories();
        $filter['countries']        = $this->getCountry();

        return $filter;
    }

    private function getCategories(): array
    {
        $db = db_connect();
        $query = $db->table($this->table);
        $res = $query
            ->select('distinct(vehicle_category_id) AS name')
            ->get();

        return $res->getResult();
    }

    private function getSubcategories(): array
    {
        $db = db_connect();
        $query = $db->table($this->table);
        $res = $query
            ->select('distinct(vehicle_subcategory_id) AS name')
            ->get();

        return $res->getResult();
    }

    private function getCountry(): array
    {
        $db = db_connect();
        $query = $db->table('country');
        $res = $query
            ->select('country_id, country_name')
            ->get();

        return $res->getResult();
    }
    #endregion

    private function checkTerm($search)
    {
        $searchData = explode( ',', $search);

        if(count($searchData) > 1)
            return $searchData[0];
        else
            return $search;
    }

    /**
     * @param $filter
     * @param \CodeIgniter\Database\BaseBuilder $query
     * @return array
     */
    public function getRes($filter, \CodeIgniter\Database\BaseBuilder $query): array
    {
        if (!empty($filter)) {
            if (isset($filter[Filter::Category]) && !empty($filter[Filter::Category]))
                $query->where("vehicle_category_id", $filter[Filter::Category]);

            if (isset($filter[Filter::Subcategory]) && !empty($filter[Filter::Subcategory]))
                $query->where("vehicle_subcategory_id", $filter[Filter::Subcategory]);

            if (isset($filter[Filter::Country]) && !empty($filter[Filter::Country]))
                $query->where("vehicle_country_id", $filter[Filter::Country]);
        }

        return $query->get()->getResult();
    }
}
