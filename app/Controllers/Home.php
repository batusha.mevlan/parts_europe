<?php

namespace App\Controllers;

use App\Models\Vehicle;
use App\Models\Parts;

class Home extends BaseController
{
    public function index()
    {
        $parts = new Parts();
        $data = $parts->getData();
        if (!$data)
            $data = [];

        $vehicle = new Vehicle();
        $filter = $vehicle->getFilterData();

        return view('index', ['data' => $data, 'filter' => $filter ]);
    }

}
