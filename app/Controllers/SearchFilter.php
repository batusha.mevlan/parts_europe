<?php

namespace App\Controllers;
use App\Models\Search;
use App\Models\Parts;
use App\Models\Vehicle;

use Filter;

class SearchFilter extends BaseController
{
    public function __construct()
    {
    }

    public function search()
    {
        $request = service('request');
        if ($request->isAJAX()) {
            $search         = new Search();
            $search_input   = $request->getGet('term');
            $filter         = [];

            $filterData     = $this->checkAndGetFilterData();
            if ($filterData['status'])
                $filter = $filterData['data'];

            $result = $search->partsVehicle($search_input, $filter);
            if($result) {
                $view = view('parts/items', ['data' => $result]);
                return json_encode(['status' => 1, 'csrf' => csrf_hash(), 'view' => $view]);
            }
            else
                return json_encode(['status' => 0, 'csrf' => csrf_hash(), 'message' => "There is no data for this vehicle!"]);
        }
        else
            return json_encode(['status'=> 0, 'message' => "Something went wrong!"]);
    }

    public function searchAutocomplete()
    {
        $request = service('request');
        if ($request->isAJAX()) {
            $vehicle = new Vehicle();
            $search_input = $request->getGet('term');
            $result = $vehicle->searchVehicle($search_input);

            return json_encode($result);

        }
        return [];
    }

    public function loadMore(): string
    {
        $request = service('request');
        if ($request->isAJAX()) {
            $search         = new Search();
            $search_input   = $request->getGet('term');
            $page           = $request->getGet('page');

            $filter         = [];
            $filterData     = $this->checkAndGetFilterData();
            if ($filterData['status'])
                $filter = $filterData['data'];

            $result = $search->more($search_input, $filter, $page);

            return view('parts/items', ['data' => $result]);
        }
        return '';
    }

    private function checkAndGetFilterData(): array
    {
        $status     = false;
        $formData   = [];
        $request    = service('request');

        if($request->getGet('filter')) {

            $filter = $request->getGet('filter');
            parse_str($filter, $formData);
            $status = true;
        }

        return [
            'status'    => $status,
            'data'      => $formData
        ];
    }
}