<div id="pe_config_container"  class="container-fluid bg-light p-3"  style="display: none">
    <div class="container">
        <div class="row">
            <div class="col-md-6 px-4 pt-3">
                <label for="">Category</label>
                <select class="custom-select" name="category">
                    <option value="">--SELECT--</option>
                    <?php foreach ($categories as $category): ?>
                        <option value="<?= $category->name ?>"><?= $category->name ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-6 px-4 pt-3">
                <label for="">Subcategory</label>
                <select class="custom-select" name="subcategory">
                    <option value="">--SELECT--</option>
                    <?php foreach ($subcategories as $subcategory): ?>
                        <option value="<?= $subcategory->name ?>"><?= $subcategory->name ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-6  px-4 pt-3">
                <label for="">Country</label>
                <select class="custom-select" name="country">
                    <option value="">--SELECT--</option>
                    <?php foreach ($countries as $country): ?>
                        <option value="<?= $country->country_id ?>"><?= $country->country_name ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-md-6  px-4 pt-3 d-flex align-items-end">
                <div class="form-check mb-2">
                    <input class="form-check-input" type="checkbox" id="activeCheck" name="is_not_active">
                    <label class="form-check-label" for="activeCheck">
                        Not Active (Parts)
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>